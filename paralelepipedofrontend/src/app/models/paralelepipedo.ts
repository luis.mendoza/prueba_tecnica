export class Paralelepipedo {
  id: string;
  alto: number;
  ancho: number;
  fondo: number;
  volumen: number;
  fecha: string;
}
