# Prueba Tecnica

## Consideraciones

Pense en guardar la fecha como identificador, pero utilice el identicador por defecto de MongoDb en la Api.

El nombre de la Base de Datos en MongoDB es prueba_tecnica

La fecha es almacenada en formato ISO, con hora UTC +0 Internacional. 

## Como Ejecutarla

1.-Instalar las siguientes aplicaciones:

Java,
Maven,
mongoDB,
NodeJS

2.-Configurar las variables de entorno de las aplicaciones mencionadas

3.-Bajar proyecto desde el repositorio: 

git clone https://gitlab.com/luis.mendoza/prueba_tecnica.git

4.-Ejecutar CMD como Administrador con el comando:

mongod

5.-Ejecutar CMD como Administrador con el comando:
mongo

5.-Ejecutar CMD como Administrador, en la ruta del proyecto de back-end con el comando:

mvn spring-boot:run

6.-Ejecutar CMD como Administrador, en la ruta del proyecto de front-end con los comandos:

npm install 

ng serve

7.-Abrir Navegador en http://localhost:4200 para ejecular app web realizada en Angular 8

Notas: 
La api se puede ejecutar en un cliente tipo Postman:
POST http://localhost:8080/V1/paralelepipedo/
{
    "alto" : 30,
    "ancho" : 30,
    "fondo" : 30
  }

Responde:
{
    "id": "5e5362336e34fa49ca635e0d"
}

GET http://localhost:8080/V1/paralelepipedo/5e5362336e34fa49ca635e0d

Responde:
{
    "volumen": 27000.0
}
