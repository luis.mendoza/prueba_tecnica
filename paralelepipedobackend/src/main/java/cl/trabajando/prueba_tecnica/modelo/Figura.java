package cl.trabajando.prueba_tecnica.modelo;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;


@Document(collection = "figura")
public class Figura {
    @Id
    private ObjectId id;
    private float alto;
    private float ancho;
    private float fondo;
    private float volumen;
    private LocalDateTime fecha;

    public Figura() {

    }

    public Figura(ObjectId id, float alto, float ancho, float fondo, float volumen, LocalDateTime fecha) {
        this.id = id;
        this.alto = alto;
        this.ancho = ancho;
        this.fondo = fondo;
        this.volumen = volumen;
        this.fecha = fecha;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public float getAlto() {
        return alto;
    }

    public void setAlto(float alto) {
        this.alto = alto;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getFondo() {
        return fondo;
    }

    public void setFondo(float fondo) {
        this.fondo = fondo;
    }

    public float getVolumen() {
        return volumen;
    }

    public void setVolumen(float volumen) {
        this.volumen = volumen;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }
}
