import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalcularComponent } from './components/calcular/calcular.component';
import { IngresarComponent } from './components/ingresar/ingresar.component';
import {HomeComponent} from './components/home/home.component';



const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'ingresar', component: IngresarComponent },
  { path: 'calcular', component: CalcularComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
