package cl.trabajando.prueba_tecnica.repositorio;

import cl.trabajando.prueba_tecnica.modelo.Figura;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FiguraRepositorio extends MongoRepository<Figura, String> {
    Figura findById(ObjectId id);

}
