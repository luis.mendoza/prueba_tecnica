package cl.trabajando.prueba_tecnica.controlador;

import cl.trabajando.prueba_tecnica.modelo.Figura;
import cl.trabajando.prueba_tecnica.repositorio.FiguraRepositorio;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/paralelepipedo")
public class FiguraControlador {
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private FiguraRepositorio repositorio;

    /*@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Figura getFiguraById(@PathVariable("id") ObjectId id) {
        return repositorio.findById(id);
    }*/

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)//, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Float> getFiguraById(@PathVariable("id") ObjectId id) {
        HashMap<String, Float> volumen = new HashMap<>();
        volumen.put("volumen", repositorio.findById(id).getVolumen());
        return volumen;
    }

    //Mejor Opcion ObjectNode
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ObjectNode createFigura(@Valid @RequestBody Figura figura) {
        figura.setId(ObjectId.get());
        figura.setFecha(LocalDateTime.now());
        figura.setVolumen(figura.getAlto()*figura.getAncho()*figura.getAncho());
        repositorio.save(figura);
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put("id", figura.getId());
        return objectNode;
    }

}
