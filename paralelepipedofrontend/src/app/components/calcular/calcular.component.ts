import { ApiService } from '../../services/api.service';
import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Paralelepipedo} from '../../models/paralelepipedo';

@Component({
  selector: 'app-calcular',
  templateUrl: './calcular.component.html',
  styleUrls: ['./calcular.component.css']
})
export class CalcularComponent implements OnInit {
  data: Paralelepipedo;
  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.data = new Paralelepipedo();
  }

  ngOnInit(): void {
  }

  submitForm(id) {
    this.apiService.getFigura(id).subscribe((response) => {
      this.data.volumen = response.volumen;
    });
  }
}
