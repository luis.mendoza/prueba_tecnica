import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Observable, of, throwError } from 'rxjs';
import { map, catchError, tap, retry } from 'rxjs/operators';
import {Paralelepipedo} from '../models/paralelepipedo';



const url = 'http://localhost:8080/V1/paralelepipedo/';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  base = 'http://localhost:8080/V1/paralelepipedo/';
  constructor(
    private http: HttpClient
  ) { }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
  postFigura(item): Observable<any> {
    return this.http.post<any>(this.base, JSON.stringify(item), this.httpOptions).pipe(retry(2), catchError(this.handleError));
  }

  getFigura(id): Observable<any> {
    return this.http.get(url + id).pipe(
      map(this.extractData));
  }
  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
