import { Component, OnInit } from '@angular/core';
import {Paralelepipedo} from '../../models/paralelepipedo';
import {ApiService} from '../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css']
})
export class IngresarComponent implements OnInit {

  data: Paralelepipedo;

  constructor(
    private apiService: ApiService,
    public router: Router
  ) {
    this.data = new Paralelepipedo();
  }

  ngOnInit(): void {
  }
  submitForm() {
    this.apiService.postFigura(this.data).subscribe((response) => {
    this.data.id = response.id;
    });
  }

}
